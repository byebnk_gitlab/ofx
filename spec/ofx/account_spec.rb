# frozen_string_literal: true

require 'spec_helper'

describe OFX::Account do
  let(:ofx) { OFX::Parser::Base.new('spec/fixtures/sample.ofx') }
  let(:parser) { ofx.parser }
  let(:account) { parser.account }

  describe 'account' do
    it 'returns currency' do
      expect(account.currency).to eq('BRL')
    end

    it 'returns bank id' do
      expect(account.bank_id).to eq('0356')
    end

    it 'returns id' do
      expect(account.id).to eq('03227113109')
    end

    it 'returns type' do
      expect(account.type).to eq(:checking)
    end

    it 'returns transactions' do
      expect(account.transactions).to be_a_kind_of(Array)
      expect(account.transactions.size).to eq(36)
    end

    it 'returns balance' do
      expect(account.balance.amount).to eq(BigDecimal('598.44'))
    end

    it 'returns balance in pennies' do
      expect(account.balance.amount_in_pennies).to eq(59_844)
    end

    it 'returns balance date' do
      expect(account.balance.posted_at).to eq(Time.gm(2009, 11, 1))
    end

    describe 'available_balance' do
      it 'returns available balance' do
        expect(account.available_balance.amount).to eq(BigDecimal('1555.99'))
      end

      it 'returns available balance in pennies' do
        expect(account.available_balance.amount_in_pennies).to eq(155_599)
      end

      it 'returns available balance date' do
        expect(account.available_balance.posted_at).to eq(Time.gm(2009, 11, 1))
      end

      it 'returns nil if AVAILBAL not found' do
        ofx = OFX::Parser::Base.new('spec/fixtures/utf8.ofx')
        parser = ofx.parser
        account = parser.account
        expect(account.available_balance).to be_nil
      end
    end

    describe 'Credit Card' do
      let(:ofx) { OFX::Parser::Base.new('spec/fixtures/creditcard.ofx') }
      let(:parser) { ofx.parser }
      let(:account) { parser.account }

      it 'returns id' do
        expect(account.id).to eq('XXXXXXXXXXXX1111')
      end

      it 'returns currency' do
        expect(account.currency).to eq('USD')
      end
    end

    describe 'With Issue' do # Bradesco do not provide a valid date in balance
      let(:ofx) { OFX::Parser::Base.new('spec/fixtures/dtsof_balance_issue.ofx') }
      let(:parser) { ofx.parser }
      let(:account) { parser.account }

      it 'returns nil for date balance' do
        expect(account.balance.posted_at).to be_nil
      end
    end

    describe 'Invalid Dates' do
      let(:ofx) { OFX::Parser::Base.new('spec/fixtures/bradesco.ofx') }
      let(:parser) { ofx.parser }

      it 'does not raise error when balance has date zero' do
        expect { parser.account.balance }.not_to raise_error
      end

      it 'returns NIL in balance.posted_at when balance date is zero' do
        expect(parser.account.balance.posted_at).to be_nil
      end
    end

    describe 'decimal values using a comma' do
      let(:ofx) { OFX::Parser::Base.new('spec/fixtures/santander.ofx') }
      let(:parser) { ofx.parser }
      let(:account) { parser.account }

      it 'returns balance' do
        expect(account.balance.amount).to eq(BigDecimal('348.29'))
      end

      it 'returns balance in pennies' do
        expect(account.balance.amount_in_pennies).to eq(34_829)
      end

      describe 'available_balance' do
        it 'returns available balance' do
          expect(account.available_balance.amount).to eq(BigDecimal('2415.87'))
        end

        it 'returns available balance in pennies' do
          expect(account.available_balance.amount_in_pennies).to eq(241_587)
        end
      end
    end
  end
end
