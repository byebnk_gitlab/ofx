# frozen_string_literal: true

require 'spec_helper'

describe OFX::Transaction do
  let(:ofx) { OFX::Parser::Base.new('spec/fixtures/sample.ofx') }
  let(:parser) { ofx.parser }
  let(:account) { parser.account }

  describe 'debit' do
    let(:transaction) { account.transactions[0] }

    it 'sets amount' do
      expect(transaction.amount).to eq(BigDecimal('-35.34'))
    end

    it 'casts amount to BigDecimal' do
      expect(transaction.amount.class).to eq(BigDecimal)
    end

    it 'sets amount in pennies' do
      expect(transaction.amount_in_pennies).to eq(-3534)
    end

    it 'sets fit id' do
      expect(transaction.fit_id).to eq('200910091')
    end

    it 'sets memo' do
      expect(transaction.memo).to eq('COMPRA VISA ELECTRON')
    end

    it 'sets check number' do
      expect(transaction.check_number).to eq('0001223')
    end

    it 'has date' do
      expect(transaction.posted_at).to eq(Time.gm(2009, 10, 9, 8))
    end

    it 'has user date' do
      expect(transaction.occurred_at).to eq(Time.gm(2009, 9, 9, 8))
    end

    it 'has type' do
      expect(transaction.type).to eq(:debit)
    end

    it 'has sic' do
      expect(transaction.sic).to eq('5072')
    end
  end

  describe 'credit' do
    let(:transaction) { account.transactions[1] }

    it 'sets amount' do
      expect(transaction.amount).to eq(BigDecimal('60.39'))
    end

    it 'sets amount in pennies' do
      expect(transaction.amount_in_pennies).to eq(6039)
    end

    it 'sets fit id' do
      expect(transaction.fit_id).to eq('200910162')
    end

    it 'sets memo' do
      expect(transaction.memo).to eq('DEPOSITO POUP.CORRENTE')
    end

    it 'sets check number' do
      expect(transaction.check_number).to eq('0880136')
    end

    it 'has date' do
      expect(transaction.posted_at).to eq(Time.parse('2009-10-16 08:00:00 +0000'))
    end

    it 'has user date' do
      expect(transaction.occurred_at).to eq(Time.parse('2009-09-16 08:00:00 +0000'))
    end

    it 'has type' do
      expect(transaction.type).to eq(:credit)
    end

    it 'has empty sic' do
      expect(transaction.sic).to eq('')
    end
  end

  context 'with more info' do
    let(:transaction) { account.transactions[2] }

    it 'sets payee' do
      expect(transaction.payee).to eq('Pagto conta telefone')
    end

    it 'sets check number' do
      expect(transaction.check_number).to eq('000000101901')
    end

    it 'has date' do
      expect(transaction.posted_at).to eq(Time.parse('2009-10-19 12:00:00 -0300'))
    end

    it 'has user date' do
      expect(transaction.occurred_at).to eq(Time.parse('2009-10-17 12:00:00'))
    end

    it 'has type' do
      expect(transaction.type).to eq(:other)
    end

    it 'has reference number' do
      expect(transaction.ref_number).to eq('101.901')
    end
  end

  context 'with name' do
    let(:transaction) { account.transactions[3] }

    it 'sets name' do
      expect(transaction.name).to eq('Pagto conta telefone')
    end
  end

  context 'with other types' do
    let(:ofx) { OFX::Parser::Base.new('spec/fixtures/bb.ofx') }
    let(:parser) { ofx.parser }
    let(:account) { parser.account }

    it 'returns dep' do
      transaction = account.transactions[9]
      expect(transaction.type).to eq(:dep)
    end

    it 'returns xfer' do
      transaction = account.transactions[18]
      expect(transaction.type).to eq(:xfer)
    end

    it 'returns cash' do
      transaction = account.transactions[45]
      expect(transaction.type).to eq(:cash)
    end

    it 'returns check' do
      transaction = account.transactions[0]
      expect(transaction.type).to eq(:check)
    end
  end

  describe 'decimal values using a comma' do
    let(:ofx) { OFX::Parser::Base.new('spec/fixtures/santander.ofx') }
    let(:parser) { ofx.parser }
    let(:account) { parser.account }

    context 'when debit' do
      let(:transaction) { account.transactions[0] }

      it 'sets amount' do
        expect(transaction.amount).to eq(BigDecimal('-11.76'))
      end

      it 'sets amount in pennies' do
        expect(transaction.amount_in_pennies).to eq(-1176)
      end
    end

    context 'when credit' do
      let(:transaction) { account.transactions[3] }

      it 'sets amount' do
        expect(transaction.amount).to eq(BigDecimal('47.01'))
      end

      it 'sets amount in pennies' do
        expect(transaction.amount_in_pennies).to eq(4701)
      end
    end
  end
end
