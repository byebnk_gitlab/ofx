# frozen_string_literal: true

require 'spec_helper'

describe OFX::SignOn do
  let(:ofx) { OFX::Parser::Base.new('spec/fixtures/creditcard.ofx') }
  let(:parser) { ofx.parser }
  let(:sign_on) { parser.sign_on }

  describe 'sign_on' do
    it 'returns language' do
      expect(sign_on.language).to eq('ENG')
    end

    it 'returns Financial Institution ID' do
      expect(sign_on.fi_id).to eq('24909')
    end

    it 'returns Financial Institution Name' do
      expect(sign_on.fi_name).to eq('Citigroup')
    end

    it 'returns status' do
      expect(sign_on.status).to be_a(OFX::Status)
    end
  end
end
