# frozen_string_literal: true

require 'spec_helper'

describe OFX::Parser do
  let(:ofx) { OFX::Parser::Base.new('spec/fixtures/sample.ofx') }

  it 'accepts file path' do
    ofx = OFX::Parser::Base.new('spec/fixtures/sample.ofx')
    expect(ofx.content).not_to be_nil
  end

  it 'accepts file handler' do
    file = File.open('spec/fixtures/sample.ofx')
    ofx = OFX::Parser::Base.new(file)
    expect(ofx.content).not_to be_nil
  end

  it 'accepts file content' do
    file = File.read('spec/fixtures/sample.ofx')
    ofx = OFX::Parser::Base.new(file)
    expect(ofx.content).not_to be_nil
  end

  it 'sets content' do
    expect(ofx.content).to eq(File.read('spec/fixtures/sample.ofx'))
  end

  it 'works with UTF8 and Latin1 encodings' do
    ofx = OFX::Parser::Base.new('spec/fixtures/utf8.ofx')
    expect(ofx.content).to eq(File.read('spec/fixtures/utf8.ofx'))
  end

  it 'sets body' do
    expect(ofx.body).not_to be_nil
  end

  it 'raises exception when trying to parse an unsupported OFX version' do
    expect do
      OFX::Parser::Base.new('spec/fixtures/invalid_version.ofx')
    end.to raise_error(OFX::UnsupportedFileError)
  end

  it 'raises exception when trying to parse an invalid file' do
    expect do
      OFX::Parser::Base.new('spec/fixtures/avatar.gif')
    end.to raise_error(OFX::UnsupportedFileError)
  end

  it 'uses 211 parser to parse version 200 ofx files' do
    allow(OFX::Parser::OFX211).to receive(:new).and_return('ofx-211-parser')
    ofx = OFX::Parser::Base.new(ofx_2_example('200'))
    expect(ofx.parser).to eq('ofx-211-parser')
  end

  it 'uses 211 parser to parse version 202 ofx files' do
    allow(OFX::Parser::OFX211).to receive(:new).and_return('ofx-211-parser')
    ofx = OFX::Parser::Base.new(ofx_2_example('202'))
    expect(ofx.parser).to eq('ofx-211-parser')
  end

  describe 'headers' do
    it 'has OFXHEADER' do
      expect(ofx.headers['OFXHEADER']).to eq('100')
    end

    it 'has DATA' do
      expect(ofx.headers['DATA']).to eq('OFXSGML')
    end

    it 'has VERSION' do
      expect(ofx.headers['VERSION']).to eq('102')
    end

    it 'has SECURITY' do
      expect(ofx.headers).to have_key('SECURITY')
      expect(ofx.headers['SECURITY']).to be_nil
    end

    it 'has ENCODING' do
      expect(ofx.headers['ENCODING']).to eq('USASCII')
    end

    it 'has CHARSET' do
      expect(ofx.headers['CHARSET']).to eq('1252')
    end

    it 'has COMPRESSION' do
      expect(ofx.headers).to have_key('COMPRESSION')
      expect(ofx.headers['COMPRESSION']).to be_nil
    end

    it 'has OLDFILEUID' do
      expect(ofx.headers).to have_key('OLDFILEUID')
      expect(ofx.headers['OLDFILEUID']).to be_nil
    end

    it 'has NEWFILEUID' do
      expect(ofx.headers).to have_key('NEWFILEUID')
      expect(ofx.headers['NEWFILEUID']).to be_nil
    end

    it 'parses headers with CR and without LF' do
      ofx = OFX::Parser::Base.new(ofx_with_carriage_return)
      expect(ofx.headers.size).to eq(9)
    end
  end

  def ofx_with_carriage_return
    header = %w[
      OFXHEADER:100 DATA:OFXSGML VERSION:102 SECURITY:NONE
      ENCODING:USASCII CHARSET:1252 COMPRESSION:NONE OLDFILEUID:NONE
      NEWFILEUID:NONE
    ].join("\r")

    body = File.read('spec/fixtures/sample.ofx').split(/<OFX>/, 2)[1]
    "#{header}\r<OFX>#{body}"
  end

  def ofx_2_example(version)
    <<~ENDOFX
      <?xml version="1.0" encoding="US-ASCII"?>
      <?OFX OFXHEADER="200" VERSION="#{version}" SECURITY="NONE" OLDFILEUID="NONE" NEWFILEUID="NONE"?>"
      <OFX>
      </OFX>
    ENDOFX
  end
end
