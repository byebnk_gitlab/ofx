# frozen_string_literal: true

require 'spec_helper'

module OFX
  module Parser
    class OFX102
      include RSpec::Matchers
    end
  end
end

describe OFX do
  describe '#OFX' do
    it 'yields an OFX instance' do
      OFX('spec/fixtures/sample.ofx') do |ofx|
        expect(ofx.class).to eq(OFX::Parser::OFX102)
      end
    end

    it 'is an OFX instance' do
      OFX('spec/fixtures/sample.ofx') do
        expect(self.class).to eq(OFX::Parser::OFX102)
      end
    end

    it 'returns parser' do
      expect(OFX('spec/fixtures/sample.ofx').class).to eq(OFX::Parser::OFX102)
    end
  end
end
