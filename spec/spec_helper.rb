# frozen_string_literal: true

require 'ofx'

RSpec::Matchers.define :have_key do |key|
  match do |hash|
    hash.respond_to?(:keys) && hash.keys.is_a?(Array) && hash.key?(key)
  end
end
