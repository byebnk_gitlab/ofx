# frozen_string_literal: true

module OFX
  class Transaction < Foundation
    attr_accessor :amount, :amount_in_pennies, :check_number, :fit_id, :memo,
                  :name, :payee, :posted_at, :occurred_at, :ref_number, :type,
                  :sic
  end
end
