# frozen_string_literal: true

module OFX
  class Statement < Foundation
    attr_accessor :account, :available_balance, :balance, :currency, :start_date, :end_date, :transactions
  end
end
