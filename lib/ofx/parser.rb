# frozen_string_literal: true

module OFX
  module Parser
    class Base
      attr_reader :headers, :body, :content, :parser

      def initialize(resource)
        resource = open_resource(resource)

        @content = convert_to_utf8(resource.read)
        @headers, @body = prepare(content)

        @parser = which_parser(headers['VERSION']).new(headers: headers, body: body)
      rescue StandardError
        raise OFX::UnsupportedFileError
      end

      def open_resource(resource)
        File.open(resource)
      rescue StandardError
        StringIO.new(resource)
      end

      private

      def which_parser(version)
        case version
        when /102/
          OFX102
        when /103/
          OFX103
        when /200|202|211|220/
          OFX211
        else
          raise OFX::UnsupportedFileError
        end
      end

      def prepare(content)
        # split headers & body
        header_text, body = content.dup.split(/<OFX>/, 2)

        raise OFX::UnsupportedFileError unless body

        # Header format is different between versions. Give each
        # parser a chance to parse the headers.
        headers = nil

        OFX::Parser.constants.grep(/OFX/).each do |name|
          headers = OFX::Parser.const_get(name).parse_headers(header_text)
          break if headers
        end

        # Replace body tags to parse it with Nokogiri
        body.gsub!(/>\s+</m, '><')
        body.gsub!(/\s+</m, '<')
        body.gsub!(/>\s+/m, '>')
        body.gsub!(/<(\w+?)>([^<]+)/m, '<\1>\2</\1>')

        [headers, body]
      end

      def convert_to_utf8(string)
        return string if Kconv.isutf8(string)

        string.encode('UTF-8', 'ISO-8859-1')
      end
    end
  end
end
