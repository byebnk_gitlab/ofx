# frozen_string_literal: true

module OFX
  class Account < Foundation
    attr_accessor :balance, :bank_id, :currency, :id, :transactions, :type, :available_balance
  end
end
