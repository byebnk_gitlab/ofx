# frozen_string_literal: true

module OFX
  class Balance < Foundation
    attr_accessor :amount, :amount_in_pennies, :posted_at
  end
end
