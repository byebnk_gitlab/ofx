# frozen_string_literal: true

module OFX
  class SignOn < Foundation
    attr_accessor :language, :fi_id, :fi_name, :status
  end
end
