# frozen_string_literal: true

module OFX
  # Error Reporting Aggregate
  class Status < Foundation
    attr_accessor :code, :severity, :message     # Error code # Severity of the error  # Textual explanation

    def success?
      code.zero?
    end
  end
end
