# frozen_string_literal: true

module OFX
  class UnsupportedFileError < StandardError; end
end
